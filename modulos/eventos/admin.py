from django.contrib import admin
from modulos.eventos.models import *

class EventosAdmin(admin.ModelAdmin):
    list_display = ('titulo','categoria','status')
    search_fields = ('titulo','categoria','status')
    list_filter = ('titulo','categoria','status')
admin.site.register(Eventos, EventosAdmin)

class CategoriasAdmin(admin.ModelAdmin):
    list_display = ('nombre',)
    search_fields = ('nombre',)
    list_filter = ('nombre',)
admin.site.register(Categorias, CategoriasAdmin)
