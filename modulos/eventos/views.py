from django.views.generic import CreateView,ListView
from modulos.eventos.models import *
from django.http import Http404

class Crear(CreateView):
    model = Eventos
    template_name = 'crear.html'
    success_url = '/inicio/'

class Ver(ListView):
    template_name = 'ver.html'
    def queryset(self):
        if 'id-evento' in self.request.GET:
            if self.request.GET['id-evento'].isnumeric():
                if Eventos.objects.filter(status=True,id=self.request.GET['id-evento']).exists():
                    consulta = Eventos.objects.filter(status=True,id=self.request.GET['id-evento'])
                else:
                    raise Http404
            else:
                raise Http404
        else:
            raise Http404
        return consulta

class Listar(ListView):
    template_name = 'listar.html'
    queryset = Eventos.objects.filter(status=True).order_by('-Fecha')
