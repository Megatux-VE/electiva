from django.db import models
from django.conf import settings

class Categorias(models.Model):
    nombre = models.CharField(max_length=25)
    class Meta:
        db_table='categorias'
        verbose_name_plural='Categorias'
    def __unicode__(self):
        return "%s" % (self.nombre)


class Eventos(models.Model):
    titulo = models.CharField(max_length=200)
    Fecha = models.DateField()
    HoraInicio = models.TimeField()
    HoraFin = models.TimeField()
    categoria = models.ForeignKey(Categorias) 
    informacion = models.TextField()
    descripcion = models.TextField()
    autor = models.CharField(max_length=80)
    direccion = models.TextField()
    patrocinadores = models.TextField()
    status = models.BooleanField(default=False)

    class Meta:
        db_table='eventos'
        verbose_name_plural='Eventos'
    def __unicode__(self):
        return "%s" % (self.titulo)
