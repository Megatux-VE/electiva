from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from modulos.inicio.views import Inicio
from modulos.eventos.views import Crear,Ver,Listar
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$',RedirectView.as_view(url='/inicio/')),
    url(r'^inicio/$',Inicio.as_view()),
    url(r'^eventos/$',Listar.as_view()),
    url(r'^eventos/ver/$',Ver.as_view()),
    url(r'^eventos/crear/$',Crear.as_view()),
    url(r'^admin/', include(admin.site.urls)),
)
