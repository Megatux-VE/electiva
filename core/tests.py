"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""


"""
class IndexTestCase(TestCase):
    def setUp(self):
        self.client=client.Client()
    def test_index(self):
        Prueba que se esta cargando la plantilla index.html
        respuesta=self.client.get('/')

        self.assertTemplateUsed(respuesta,'index.html')
"""
from core.models import *
import random
from django.test import TestCase
def crearMensajes(contacto,linea,cantMsg=1,tipo='env',status='pend'):
    print ("crear mensajes")
    for ciclo in range(0,cantMsg):
        msg=MensajeTxt(mensaje='mensaje %s'%(str(random.random())))
        msg.save()
        msg=Mensajes(contacto=contacto,linea=linea,mensaje=msg,tipo=tipo,status=status)
        msg.save()
    #MensajeTxt.objects.bulk_create(txt)
    
        
from django.contrib.auth import get_user_model

from django.contrib.auth.models import Permission
class ModelTestCase(TestCase):
    def setUp(self):
        self.usuario=get_user_model().objects.create_user(username='hackercito',email='hackel@foo',password='123456',)
        self.usuario.save()
        telf=Telf.objects.create(telf='123',usuario=self.usuario)
        telf.save()#Usuario del sistema
        telf.save()
        self.contacto,creado=Contactos.objects.get_or_create(telf=telf,nombre='contacto',apellido='foo',email='contacto@foo',)
        self.contacto.save()#Contacto del sistema

# Crear un contacto en la libreta
        telf=Telf.objects.create(telf='412234454',usuario=self.usuario)
        telf.save()
        contacto,creado=Contactos.objects.get_or_create(telf=telf,nombre='contacto2',apellido='foo',email='contacto@foo',)
        contacto.save()#Contacto del sistema
        lib=Libretas.objects.create(usuario=self.usuario)
        LibretaContacto(libreta=lib,contacto=contacto).save()
        #Crear Lineas
        self.lineas=[]
        for telf in range (4):
            linea=Lineas.objects.get_or_create(numero=telf)[0]
            self.lineas.append(linea.id)

        plan=self.usuario.createPlan(self.lineas,500)
        
    def test_api(self):
        from django.test.client import Client
        c = Client()
        c.login(username=self.usuario, password='123456')
        response = c.get('/comma/api/v0.1/core/contactos/')
        print(response.content)

        self.assertEqual(response.content,b'[{"pk": 2, "telf": "412234454", "etiquetas": "", "nombre": "contacto2", "apellido": "foo", "email": "contacto@foo"}]')

    def test_createPlan_duplicado(self):
        """
        Prueba que no se pueda crear planes idénticos
        """
        lineas=[]
        from django.core.exceptions import ValidationError
        for telf in range (4):
            linea=Lineas.objects.get_or_create(numero=telf)[0]
            lineas.append(linea.id)
        self.assertRaisesRegex(ValidationError,'Ya existe un plan igual',self.usuario.createPlan,lineas,500)
    def test_getLinea(self):
        cantMsgs=[100,100,100,90]#La 4ta linea tienen menos mensajes
        usuario=get_user_model().objects.create_user('foo','foo@foo.bar','123')
        plan=Planes.objects.create(cuota=300,creador=usuario)
        usuario.addPlan(plan,self.usuario)
        for i in range(0,4):
            linea,creada=Lineas.objects.get_or_create(numero='123',tipo='reg',dedicada=False,activa=True)
            PlanesLineas.objects.create(plan=plan,linea=linea)
            crearMensajes(self.contacto,linea,cantMsgs[i])
        linea=self.usuario.getLinea()

    def test_getLinea_limitecuota(self):
        """ Evalua el limite de cupos """
        pass
        








